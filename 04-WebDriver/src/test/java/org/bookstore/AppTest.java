package org.bookstore;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;

public class AppTest {

    WebDriver driver;

    @Before
    public void setUp(){
        System.setProperty("webdriver.chrome.driver", "/Users/jbandi/Temp/chromedriver");
        driver = new ChromeDriver();
    }

    @Test
    public void testApp() throws InterruptedException {

        driver.get("http://localhost:4200/catalog");

        WebElement searchInput = driver.findElement(By.id("q"));
        searchInput.sendKeys("Hells");

        WebElement searchButton = driver.findElement(By.id("search"));
        searchButton.click();

        Thread.sleep(100);

        List<WebElement> albums = driver.findElements(By.xpath("//div[@id='searchResult']/li"));

        assertThat(albums.size(), is(1));

        driver.quit();
    }
}
