package org.bookstore;

import static org.assertj.core.api.Assertions.*;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class CatalogTest {

    private Catalog catalog;

    @Before
    public void setUp(){
        catalog = new Catalog();
    }

    @Test
    public void should_return_java_titles()
    {
        List<String> titles = catalog.searchTitles("Java");

        assertThat(titles.size()).isEqualTo(2);
        assertThat( titles.get(0)).isEqualTo("Java for Beginners");
    }

    @Test
    public void should_return_empty_result()
    {
        List<String> titles = catalog.searchTitles(".NET");

        assertThat(titles).isEmpty();
    }

    @Test
    public void testException() {
        Throwable thrown = catchThrowable(() -> { throw new Exception("boom!"); });

        assertThat(thrown).isInstanceOf(Exception.class)
                .hasMessageContaining("boom");
    }
}
