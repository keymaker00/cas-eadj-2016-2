package org.bookstore;

import java.util.ArrayList;
import java.util.List;

public class Catalog
{
    private String[] titles = new String[] {"Java for Beginners", "EJB in Action", "Advanced JavaEE"};


    public List<String> searchTitles(String term)
    {
        final ArrayList<String> result = new ArrayList<String>();
        for(String title : titles){
            if (title.contains(term)){
                result.add(title);
            }
        }

        return result;
    }
}
