package org.bookstore;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class CatalogTest {

    private Catalog catalog;

    @Before
    public void setUp(){
        catalog = new Catalog();
    }

    @Test
    public void should_return_java_titles()
    {
        List<String> titles = catalog.searchTitles("Java");
        assertEquals(2, titles.size());
        assertEquals("Java for Beginners", titles.get(0));
    }

    @Test
    public void should_return_empty_result()
    {
        List<String> titles = catalog.searchTitles(".NET");
        assertEquals(0, titles.size());
    }

    @Test(expected=IndexOutOfBoundsException.class)
    public void testIndexOutOfBoundsException() {
        ArrayList emptyList = new ArrayList();
        Object o = emptyList.get(0);
    }

}
