Start the standalone H2 database for integration test:

    java -cp $M2_REPO/com/h2database/h2/1.4.193/h2*.jar org.h2.tools.Server

Then enter the following properties and connect:

    JDBC URL: jdbc:h2:tcp://localhost/~/musicstore
    User Name: sa
    Empty password
    
Now you can run the test against the integration-test persistence unit.
    
