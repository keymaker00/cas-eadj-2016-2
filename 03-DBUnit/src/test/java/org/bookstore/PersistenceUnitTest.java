package org.bookstore;

import org.junit.Test;

import javax.persistence.Persistence;

public class PersistenceUnitTest {

    @Test
    public void instantiating_entity_manager_factory_should_succeed() {

        // This throws an exception if any named query is not correct
        // This throws an exception if schema and mapping-meta-data are different

        // Test against an in-memory DB
        Persistence.createEntityManagerFactory("musicstore-test");

        // Test angainst a real DB
//         Persistence.createEntityManagerFactory("musicstore-integrationtest");
    }
}
