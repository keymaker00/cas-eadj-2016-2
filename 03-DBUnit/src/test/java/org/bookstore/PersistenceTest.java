package org.bookstore;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

import org.bookstore.entities.Book;
import org.dbunit.dataset.filter.DefaultColumnFilter;
import org.junit.Before;
import org.junit.Test;
import org.dbunit.Assertion;
import org.dbunit.IDatabaseTester;
import org.dbunit.JdbcDatabaseTester;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.ReplacementDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

public class PersistenceTest {

    private EntityManagerFactory emf;
    private EntityManager em;
    private IDatabaseTester databaseTester;

    @Before
    public void setUp() throws Exception {
        // Init database
        emf = Persistence.createEntityManagerFactory("musicstore-test");
        em =  emf.createEntityManager();

        final String connectionDriver = (String) emf.getProperties().get("javax.persistence.jdbc.driver");
        final String connectionUrl = (String) emf.getProperties().get("javax.persistence.jdbc.url");
        final String connectionUser = "sa"; // The user can't be read from the properties ...
        final String connectionPassword = ""; // The password can't be read from the properties ...

        // Set up database to initial state
        databaseTester  = new JdbcDatabaseTester(connectionDriver, connectionUrl, connectionUser, connectionPassword);

        IDataSet inputDataSet = new FlatXmlDataSetBuilder().build(this.getClass().getClassLoader().getResourceAsStream("testdata/books_setup.xml"));
        ReplacementDataSet dataSet = new ReplacementDataSet(inputDataSet);
        dataSet.addReplacementObject("[NULL]", null);
        databaseTester.setDataSet(dataSet);
        databaseTester.onSetup();
    }

    @Test
    public void shouldPersistBook() throws Exception {

        List resultList = em.createQuery("select b from Book b").getResultList();
        assertThat(resultList.size(), is(1));

        em.getTransaction().begin();

        Book book = new Book();
        book.setTitle("Java in Action");
        em.persist(book);

        em.flush();
        em.getTransaction().commit();

        IDataSet databaseDataSet = databaseTester.getConnection().createDataSet();

        ITable actualTable = databaseDataSet.getTable("BOOK");
        ITable filteredTable = DefaultColumnFilter.excludedColumnsTable(actualTable, new String[]{"id"});

        IDataSet expectedDataSet = new FlatXmlDataSetBuilder().build(this.getClass().getClassLoader().getResourceAsStream("testdata/books_expected.xml"));
        ITable expectedTable = expectedDataSet.getTable("BOOK");

        Assertion.assertEquals(expectedTable, filteredTable);
    }
}
