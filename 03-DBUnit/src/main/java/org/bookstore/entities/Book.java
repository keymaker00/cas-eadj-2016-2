package org.bookstore.entities;

import javax.persistence.*;

@Entity
@NamedQueries({
        @NamedQuery(name = Book.FIND_ALL, query = "SELECT b FROM Book b"),
        @NamedQuery(name = Book.FIND_BY_ID, query = "SELECT b FROM Book b WHERE b.id = :id")})
public class Book {

    public static final String FIND_ALL = "Book.findAll";
    public static final String FIND_BY_ID = "Book.findById";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
