import { browser, element, by } from 'protractor';

export class CatalogPage {
  navigateTo() {
    return browser.get('/');
  }

  getTitleText() {
    return element(by.css('app-catalog h2')).getText();
  }

  searchFor(term: string){
    const termInput = element(by.id('q'));
    const searchButton = element(by.id('search'));

    termInput.sendKeys(term);
    searchButton.click();
  }

  getResultItems(){
    return element.all(by.css('#searchResult li'))
  }
}
