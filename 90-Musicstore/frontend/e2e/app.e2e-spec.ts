import { CatalogPage } from './app.po';

describe('Catalog', function() {
  let page: CatalogPage;

  beforeEach(() => {
    page = new CatalogPage();
    page.navigateTo();
  });

  it('should display catalog title', () => {
    expect(page.getTitleText()).toEqual('Catalog Search');
  });

  it('should display one result when searching for unique term', () => {
    page.searchFor('Hell');
    expect(page.getResultItems().count()).toEqual(1);
  });


});
