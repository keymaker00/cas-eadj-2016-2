import {Routes} from "@angular/router";

import {CatalogComponent} from "./catalog/catalog.component";
import {CartComponent} from "./cart/cart.component";
import {AccountComponent} from "./account/account.component";

export const routes: Routes = [
  {path: '', redirectTo: 'catalog', pathMatch: 'full'},
  {path: 'catalog', component: CatalogComponent},
  {path: 'cart', component: CartComponent},
  {path: 'account', component: AccountComponent}
];


