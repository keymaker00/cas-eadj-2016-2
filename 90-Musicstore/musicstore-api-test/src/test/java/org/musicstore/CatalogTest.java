package org.musicstore;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import org.junit.BeforeClass;
import org.junit.Test;

import static com.jayway.restassured.RestAssured.when;
import static org.hamcrest.Matchers.*;

public class CatalogTest {

    @BeforeClass
    public static void setUpClass() {
        RestAssured.baseURI = "http://localhost";
        RestAssured.port = 8080;
        RestAssured.basePath = "/musicstore/api";
    }

    @Test
    public void empty_search_should_return_nothing() {
        when().
                get("/albums").
                then().
                statusCode(200).
                contentType(ContentType.JSON).
                body("", is(empty())).
                body("$", hasSize(0));
    }

    @Test
    public void search_for_unique_term_should_return_nothing() {
        when().
                get("/albums?q=Hell").
                then().
                statusCode(200).
                contentType(ContentType.JSON).
                body("", is(not((empty())))).
                body("$", hasSize(1));
    }

}
