package org.music.rest;

import com.sun.xml.internal.xsom.impl.Ref;
import org.music.entity.Album;
import org.music.repository.AlbumRepository;
import org.music.repository.IAlbumRepository;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("albums")
public class AlbumResource {

    @Inject
    private AlbumRepository albumRepository;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Album> getAlbums(@QueryParam("q") String searchTerm){
        System.out.println("Getting albums for search term: " + searchTerm);
        return albumRepository.searchAlbums(searchTerm);
    }

}
