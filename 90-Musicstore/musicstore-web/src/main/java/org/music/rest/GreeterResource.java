package org.music.rest;

import org.music.Greeter;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("greeting")
public class GreeterResource {

    @Inject
    private Greeter greeter;

    @GET
    public String getGreeting(){
        return greeter.createGreeting("Web");
    }

}
