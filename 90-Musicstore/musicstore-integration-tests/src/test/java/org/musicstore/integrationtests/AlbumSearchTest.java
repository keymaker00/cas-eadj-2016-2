package org.musicstore.integrationtests;

import org.junit.Before;
import org.junit.Test;
import org.music.entity.Album;
import org.music.repository.IAlbumRepository;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.List;

import static junit.framework.Assert.*;

public class AlbumSearchTest {

    InitialContext jndi;
    private IAlbumRepository albumRepository;

    @Before
    public void setUp() throws NamingException {

        jndi = new InitialContext();
        albumRepository = (IAlbumRepository) jndi.lookup("org.music.repository.IAlbumRepository");
    }

    @Test
    public void perform_album_search_by_title(){

        List<Album> albums = albumRepository.searchAlbums("Hell");
        assertEquals(1, albums.size());
    }

}
