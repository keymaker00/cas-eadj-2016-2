# Musicstore

## Build the project

	mvn clean install -DskipTests -DskipITs

The result is an EAR in the directory `deployments`


## Set up the Application Server

Start the Database:

	asadmin start-database --dbhost=localhost --dbport=1527

Start Glassfish:

	asadmin start-domain --verbose=true

Create the domain:

    asadmin --user admin --passwordfile password.txt create-domain --savelogin true musicstore


Create the datasource:


	asadmin create-jdbc-connection-pool --datasourceclassname org.apache.derby.jdbc.ClientDataSource --restype javax.sql.DataSource --property ServerName=localhost:Port=1527:DatabaseName=musicstore:User=app:Password=app:ConnectionAttributes="'create=true'" jdbc/musicstorePool

	asadmin create-jdbc-resource --connectionpoolid jdbc/musicstorePool jdbc/musicstore


## Deploy the project into Glassfish

	asadmin deploy --force=true musicstore-ear/target/*.ear


## Start the frontend

    cd frontend
    npm start


## Test the project
    
Start WildFly for Arquillian tests:
    
    cd musicstore-ejb/target/wildfly-8.1.0.Final
    JBOSS_HOME=.
    ./bin/standalone.sh 


Run Persistence & EJB tests:

    cd musicstore-ejb
    mvn test
    
Run the API tests:

    cd musicstore-api-tests
    mvn test
    
Run the load tests:

    cd musicstore-load-tests
    mvn gatling:execute 
    
Run the frontend tests (browser automation):

    cd frontend
    npm run e2e
