package org.music.repository;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.persistence.UsingDataSet;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.music.entity.Album;

import javax.ejb.EJB;
import javax.inject.Inject;
import java.util.List;

@RunWith(Arquillian.class)
public class AlbumRepositoryTest {

    @Deployment
    public static WebArchive createDeployment() {
        WebArchive jar = ShrinkWrap.create(WebArchive.class)
                .addPackage(Album.class.getPackage())
                .addPackage(AlbumRepository.class.getPackage())
                .addAsResource("test-persistence.xml", "META-INF/persistence.xml")
                .addAsWebInfResource("musicstore-ds.xml")
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
        //System.out.println(jar.toString(true));
        return jar;
    }

    @Inject
    AlbumRepository albumRepository;

    @Test
    @UsingDataSet("datasets/albums.yml")
    public void should_find_album() {
        final List<Album> albums = albumRepository.searchAlbums("Test");
        Assert.assertEquals(1, albums.size());
    }

}

