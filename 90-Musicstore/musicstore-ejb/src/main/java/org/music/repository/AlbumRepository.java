package org.music.repository;


import org.music.entity.Album;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

//@Stateless(name = "AlbumRepository")
//public class AlbumRepository implements IAlbumRepository {

@Stateless
public class AlbumRepository {

    @PersistenceContext(unitName = "musicstore")
    private EntityManager em;

    public List<Album> searchAlbums(String term){
        final Query query = em.createQuery("select a from Album a where a.title like :likeTerm");
        query.setParameter("likeTerm", "%" + term + "%");
        return query.getResultList();
    }

}
