package org.music.repository;

import org.music.entity.Album;

import javax.ejb.Remote;
import java.util.List;

@Remote
public interface IAlbumRepository {

    public List<Album> searchAlbums(String term);

}
