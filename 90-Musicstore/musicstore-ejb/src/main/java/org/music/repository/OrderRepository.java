package org.music.repository;

import org.music.entity.MusicOrder;

import javax.ejb.Stateless;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.Serializable;
import java.util.List;

@Stateless
@Named
public class OrderRepository implements Serializable {

    @PersistenceContext(unitName="musicstore")
    private EntityManager em;

    public List<MusicOrder> getOrders(){
        List<MusicOrder> resultList = em.createNamedQuery(MusicOrder.FIND_ALL).getResultList();
        return resultList;
    }

    public List<MusicOrder> getOrdersByEmail(String email) {
        List<MusicOrder> resultList = em.createNamedQuery(MusicOrder.FIND_BY_EMAIL).setParameter("email", email).getResultList();
        return resultList;
    }
}
