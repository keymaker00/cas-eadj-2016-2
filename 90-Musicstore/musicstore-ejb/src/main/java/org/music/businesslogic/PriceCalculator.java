package org.music.businesslogic;

import org.music.repository.OrderRepository;
import org.music.entity.Album;
import org.music.entity.MusicOrder;

import javax.inject.Inject;
import java.io.Serializable;
import java.util.List;

public class PriceCalculator implements Serializable {

    public static final double DISCOUNT_FACTOR = 0.9;

    OrderRepository orderRepository;

    @Inject
    public PriceCalculator(OrderRepository musicOrderRepository) {
        this.orderRepository = musicOrderRepository;
    }

    public double calculatePrice(List<Album> albums) {
        double sum = 0;
        for(Album album : albums)
            sum += album.getPrice();

        return sum;
    }

    public double calculatePrice(MusicOrder currentOrder) {

        List<MusicOrder> previousOrders = orderRepository.getOrdersByEmail(currentOrder.getEmail());
        if (previousOrders.size() > 0)
            return currentOrder.getTotalAmount() * DISCOUNT_FACTOR;
        else
            return currentOrder.getTotalAmount();
    }
}
