package org.music;

import org.music.entity.Album;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Singleton
@Startup
public class DataSeeder {

    @PersistenceContext(unitName = "musicstore")
    private EntityManager em;

    @PostConstruct
    public void importData() {

        System.out.println("******** SEEDING DATA ***********");

        createAlbum("Hells Bells", 10, "hells_bells.jpg");
        createAlbum("Torch Songs", 12d, "torch_songs.jpg");
        createAlbum("Beautiful Garbage", 9d, "beautiful_garbage.jpg");
    }

    private Album createAlbum(String title, double price, String artUrl) {
        Album album = new Album();
        album.setTitle(title);
        persist(album);
        return album;
    }

    private void persist(Object entity) {
        em.persist(entity);
    }

}
