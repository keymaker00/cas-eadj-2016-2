package musicstore

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration._

class Catalog extends Simulation {

  val httpConf = http
    .baseURL("http://localhost:8080/musicstore/api") // Here is the root for all relative URLs
    .acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8") // Here are the common headers
    .acceptEncodingHeader("gzip, deflate")
    .acceptLanguageHeader("en-US,en;q=0.5")
    .userAgentHeader("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:16.0) Gecko/20100101 Firefox/16.0")

  val feeder = csv("search.csv").random


  val scn = scenario("Catalog Search") // A scenario is a chain of requests and pauses
    .feed(feeder)
    .exec(http("All Albums")
    .get("/albums?q=${term}"))
    .pause(7); // Note that Gatling has recorder real time pauses


  setUp(scn.inject(atOnceUsers(100)).protocols(httpConf))
}
