package org.musicstore.testhelper;

import org.music.entity.Album;

import javax.persistence.EntityManager;

public class AlbumBuilder {
    private final EntityManager em;
    private Album album;

    public AlbumBuilder(EntityManager em) {
        this.em = em;
    }

    public AlbumBuilder create(){
        this.album = new Album();
        return this;
    }

    public AlbumBuilder withName(String name){
        this.album.setTitle(name);
        return this;
    }

    public Album persist(){
        em.getTransaction().begin();
        em.persist(album);
        em.flush();
        em.getTransaction().commit();

        return album;
    }

}
