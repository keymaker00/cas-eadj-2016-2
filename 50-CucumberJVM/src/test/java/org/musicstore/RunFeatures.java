package org.musicstore;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions( plugin = {"pretty", "html:target/site/cucumber-pretty", "json:target/cucumber.json"})
//@CucumberOptions( format = { "pretty",
//        "html:target/site/cucumber-pretty",
//        "json:target/cucumber.json" })

public class RunFeatures {
}
