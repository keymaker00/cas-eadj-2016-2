package org.musicstore;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import com.sun.tools.javac.jvm.Gen;
import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.music.entity.Album;
import org.music.repository.IAlbumRepository;
import org.musicstore.testhelper.AlbumBuilder;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

public class SearchSteps {

    private IAlbumRepository albumRepository;
    private List<Album> searchResult;

    EntityManagerFactory emf;
    EntityManager em;

    @Before
    public void setUp() throws NamingException {


        emf = Persistence.createEntityManagerFactory("EnterpriseMusicStoreIntegrationTest");
        em = emf.createEntityManager();

        InitialContext jndi = new InitialContext();
        albumRepository = (IAlbumRepository) jndi.lookup("org.music.repository.IAlbumRepository");
    }

    @After
    public void afterScenario(){
        emf.close();
    }


    @Given("^the following albums:$")
    public void theFollowingAlbums(DataTable albumTable) throws Throwable {

        AlbumBuilder albumBuilder = new AlbumBuilder(em);

        List<Map<String, String>> albums = albumTable.asMaps(String.class, String.class);

        // Insert albums
        for(Map<String, String> albumMap : albums) {
            albumBuilder.create().withName(albumMap.get("name")).persist();
        }
    }

    @When("^I search for '(.*)'$")
    public void iSearchForHellsBells(String term) throws Throwable {

        searchResult = albumRepository.searchAlbums(term);
    }

    @Then("^I should find (\\d+) album.?$")
    public void iShouldFindAlbum(int count) throws Throwable {
        assertThat(searchResult.size(), is(count));
    }

    @When("^xyz$")
    public void xyz() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }
}
