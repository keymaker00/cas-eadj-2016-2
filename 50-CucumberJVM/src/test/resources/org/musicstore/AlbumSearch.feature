Feature: Album Search

  A customer should find albums based on different search criteria.

  Background:
    Given the following albums:
    |name           | genre |
    |Hells Bells    | rock  |
    |Chill Out Jazz | jazz  |
    |My Jazzy       | jazz  |
    |Black 2016     | rnb   |


  Scenario: Search by unique title
    When I search for 'Hells Bells'
    Then I should find 1 album

  Scenario: Search by generic title
    When I search for 'Jazz'
    Then I should find 2 album

#  Scenario: Search by genre
#    When I search for genre 'jazz'
#    Then I should find 2 albums
