package org.bookstore;

import java.util.ArrayList;

public class CartService {

    private PriceCalculator priceCalculator;
    private OrderService orderService;

    private ArrayList<OrderPosition> orderPositions = new ArrayList<>();

    public CartService(PriceCalculator priceCalculator, OrderService orderService) {
        this.priceCalculator = priceCalculator;
        this.orderService = orderService;
    }

    public void addPosition(Book book, int quantity){
        final Number price = priceCalculator.getPrice(book, quantity);

        orderPositions.add(new OrderPosition(book, quantity, price));
    }

    public void createOrder(){
        orderService.createOrder(orderPositions);
    }

}
