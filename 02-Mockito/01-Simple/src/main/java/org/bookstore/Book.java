package org.bookstore;

public class Book {

    public Book(String title) {
        this.title = title;
    }

    private String title;

    public String getTitle() {
        return title;
    }
}
