package org.bookstore;

public class OrderPosition {
    private final Book book;
    private final int quantity;
    private final Number price;

    public OrderPosition(Book book, int quantity, Number price) {

        this.book = book;
        this.quantity = quantity;
        this.price = price;
    }

    public Book getBook() {
        return book;
    }

    public int getQuantity() {
        return quantity;
    }

    public Number getPrice() {
        return price;
    }
}
