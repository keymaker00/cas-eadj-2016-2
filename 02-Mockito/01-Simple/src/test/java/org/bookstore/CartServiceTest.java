package org.bookstore;

import org.junit.Test;
import org.mockito.ArgumentCaptor;

import java.util.List;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

public class CartServiceTest {


    @Test
    public void should_order_two_books(){

        // *** Arrange ***
        // Record behavior with a stub
        final PriceCalculator priceCalculatorStub = mock(PriceCalculator.class);
        when(priceCalculatorStub.getPrice(any(), anyInt()))
                .thenReturn(22, 33);

        // Use a mock for later verification
        final OrderService orderServiceMock = mock(OrderService.class);

        // Set up entity under test
        CartService cartService = new CartService(priceCalculatorStub, orderServiceMock);

        // *** Act ***
        cartService.addPosition(new Book("Java in Action"), 2);
        cartService.addPosition(new Book(".NET in Action"), 1);

        cartService.createOrder();

        // *** Assert ***
        // Simple check for interaction
        verify(orderServiceMock, times(1)).createOrder(anyList());

//        // Complex check for interaction
//        final ArgumentCaptor<List<OrderPosition>> argument = ArgumentCaptor.forClass(List.class);
//        verify(orderServiceMock, times(1)).createOrder(argument.capture());
//        assertThat(argument.getValue().size(), is(equalTo(2)));
//        assertThat(argument.getValue().get(0).getPrice(), is(equalTo(22)));
//        assertThat(argument.getValue().get(1).getPrice(), is(equalTo(33)));

//        // The price calculator could also be used as a mock
//        verify(priceCalculatorStub, times(2)).getPrice(any(), anyInt());
    }

}
