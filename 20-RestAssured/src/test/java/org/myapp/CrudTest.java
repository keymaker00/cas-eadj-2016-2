package org.myapp;

import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;
import static com.jayway.restassured.RestAssured.*;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.ExtractableResponse;
import com.jayway.restassured.response.Response;
import org.junit.BeforeClass;
import org.junit.Test;

import java.time.Instant;

public class CrudTest {

    @BeforeClass
    public static void setUpClass() {
        RestAssured.baseURI = "http://localhost";
        RestAssured.port = 3456;
        RestAssured.basePath = "/todos";
    }

    @Test
    public void getAllToDos() {

        when().
            get().
        then().
            statusCode(200).
            body(is(not(emptyArray()))).
            body("[0].description", is("First ToDo"));
    }

    @Test
    public void getFirstToDos() {

        when().
            get("/1").
        then().
            statusCode(200).
            body(is(not(empty()))).
            body("description", is("First ToDo"));
    }

    @Test
    public void postToDo() {

        ToDo toDo = new ToDo();
        toDo.setDescription("Test");
        toDo.setCreated(Instant.now().toString());

        given().
            contentType("application/json").
            body(toDo).
        when().
            post().
        then().
            statusCode(201);
       }

    @Test
    public void fullCRUD() {

        ToDo toDo = new ToDo();
        toDo.setDescription("Test");
        toDo.setCreated(Instant.now().toString());

        Integer count = get().then().extract().path("size()");

        given().
            contentType("application/json").
            body(toDo).
        when().
            post().
        then().
            statusCode(201);

        ExtractableResponse<Response> response = get().then().extract();
        Integer countAfterInsert = response.path("size()");
        assertThat(countAfterInsert, is(count + 1));

        int firstToDoId = response.path("[0].id");

        ToDo updatedToDo = new ToDo();
        String newDescription = "Test" + Instant.now().toString();
        updatedToDo.setDescription(newDescription);
        updatedToDo.setCreated(Instant.now().toString());

        given().
            contentType("application/json").
            body(updatedToDo).
        when().
            put("/" + firstToDoId).
        then().
            statusCode(200);

        String description = get().then().extract().path("[0].description");
        assertThat(description, is(newDescription));

        when().
            delete("/" + firstToDoId).
        then().
            statusCode(200);


        Integer countAfterDelete = get().then().extract().path("size()");
        assertThat(countAfterDelete, is(count));
    }

}
