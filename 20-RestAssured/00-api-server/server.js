var jsonServer = require('json-server');

// Returns an Express server
var server = jsonServer.create();

// Set default middlewares (logger, static, cors and no-cache)
var middlewares = jsonServer.defaults();

var db = {
  todos: [
      {id: 1, description: 'First ToDo', created: new Date()}
  ]
};
var router = jsonServer.router(db);

server.use(middlewares);
server.use(router);

server.listen(3456);
console.log('The API server is running at http://localhost:3456');
console.log('The ToDo endpoint is running at http://localhost:3456/todos');
