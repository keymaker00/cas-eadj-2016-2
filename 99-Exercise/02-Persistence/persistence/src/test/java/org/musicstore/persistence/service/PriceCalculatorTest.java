package org.musicstore.persistence.service;

import org.jglue.cdiunit.AdditionalClasses;
import org.jglue.cdiunit.CdiRunner;
import org.jglue.cdiunit.ProducesAlternative;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.musicstore.domain.PriceCalculator;
import org.musicstore.domain.entities.MusicOrder;
import org.musicstore.domain.repositories.MusicOrderRepository;
import org.musicstore.persistence.repositories.MusicOrderRepositoryImpl;

import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.ArrayList;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(CdiRunner.class)
@AdditionalClasses(MusicOrderRepositoryImpl.class)
public class PriceCalculatorTest {

    // This test tests the business layer including the persistence layer
    // It uses a test-specific persistence unit

    @Inject
    PriceCalculator priceCalculator;

    @Produces
    @ProducesAlternative
    public EntityManager createEntityManager() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("EnterpriseMusicStoreTest");
        return emf.createEntityManager();
    }

    @Test
    public void shouldApplyStandardDiscount() throws Exception {

        Double totalAmount = 10d;

        // Set up stubs
        MusicOrder order = mock(MusicOrder.class);
        when(order.getEmail()).thenReturn("my_email");
        when(order.getTotalAmount()).thenReturn(totalAmount);

        // Trigger the actual test
        Double calculatedPrice = priceCalculator.calculatePrice(order);

        // We check the final result of price calculation
        assertThat(calculatedPrice, is(totalAmount * PriceCalculator.STANDARD_DISCOUNT_FACTOR));
    }

}
